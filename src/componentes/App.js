import React, { Component } from 'react';
import Header from './Header';
import FormularioGasto from './Formulario';
import Listado from './Listado';
import { validarPresupuesto } from './lib';
import ControlPresupuesto from './ControlPresupuesto';
import '../css/App.css';


class App extends Component {

  state = {
    presupuesto : '',
    restante : '',
    gastos: {}
  }

  componentDidMount() {
    let presupuesto = '';
    while(!validarPresupuesto(presupuesto) || !presupuesto) {
      presupuesto = this.obtenPresupuesto();
    }
    this.setState({
      presupuesto: presupuesto,
      restante: presupuesto
    });
    console.log(`El presupuesto para esta semana es $ ${presupuesto}`);
  }

  obtenPresupuesto() {
    let presupuesto = prompt('¿Cuál es tu presupuesto para esta semana?');
    return presupuesto
  }

  //Agregar gasto al state
  agregarGasto = (gasto) => {
    console.log('Desde Form se agregó: '+gasto);
    //tomar una copia del state actual
    const gastos = {...this.state.gastos}

    let key=`gasto${Date.now()}`;
    //agregr gasto al objeto state
    gastos[key] = gasto;
    console.log(gastos);
    this.setState({
      gastos
    });
    this.restarPresupuesto(gasto.cantidadGasto);
  }

  restarPresupuesto = (cgastada) => {
    let restante = this.state.restante;
    restante -= cgastada;
    this.setState({
      restante
    });
  }

  render() {
    return (
      <div className="App container">
        <Header
            titulo="Gasto Semanal"
          />
        <div className="contenido-principal contenido">
          <div className="row">
            <div className="one-half column">
              <FormularioGasto
                  agregarGasto = {this.agregarGasto}
                />
            </div>
            <div className="one-half column">
                <Listado
                  gastos={this.state.gastos}
                  />
                <ControlPresupuesto
                    presupuesto={this.state.presupuesto}
                    restante={this.state.restante}
                  />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
