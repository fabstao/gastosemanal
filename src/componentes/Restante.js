import React, { Component } from 'react';
import { revisarPresupuesto } from './lib';

class Restante extends Component {
  render(){
    return(
      <div className={revisarPresupuesto(this.props.presupuesto, this.props.restante)}>
        Restante: &nbsp; &nbsp; &nbsp; $ {this.props.restante}
      </div>
    )
  }
}

export default Restante;
